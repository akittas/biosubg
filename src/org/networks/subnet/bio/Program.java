package org.networks.subnet.bio;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Set;

import org.networks.subnet.bio.BioVertex.BioVertexStatus;
import org.networks.subnet.bio.NetSim.FilterMethod;

import edu.uci.ics.jung.graph.DirectedGraph;

public class Program {
	// simulation parameters
	public static long RUNS = (long)1E6;
	public static long ACTUAL_RUNS = 0;
	public static int STEPS = 1000;
	public static double THRES_MIN = 0.0;
	public static double THRES_MAX = 1.0;
	public static double THRES_STEP = 0;
	public static int MAX_PATHS = 1000;
	public static int MAX_HOPS = 1000;
	public static boolean MARK_SHORTEST = true;
    public static boolean PRUNE_AFTER = false;
	public static FilterMethod FILTER_METHOD = FilterMethod.NORMAL;
	public static final BioVertexStatus SOURCE_STATUS = BioVertexStatus.SOURCE;
	public static final BioVertexStatus TARGET_STATUS = BioVertexStatus.TARGET;
	public static String NET_FILE = "";
	public static String SOURCE_FILE = "";
	public static String TARGET_FILE = "";
	public static String PARAMS_FILE = "simparams.txt";

	// program parameters
	public static int origNetEdges = 0;
	public static int origNetVertices = 0;

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		switch (args.length) {
		case 0:
			System.out.println("info - Arguments unspecified, will input parameters from: " + PARAMS_FILE);
            inputParameters(PARAMS_FILE);
			break;
		case 1:
			PARAMS_FILE = args[0];
            inputParameters(PARAMS_FILE);
			break;
		case 2:
			PARAMS_FILE = args[0];
            inputParameters(PARAMS_FILE);
			NET_FILE = args[1];
			break;
		case 4:
			PARAMS_FILE = args[0];
            inputParameters(PARAMS_FILE);
			NET_FILE = args[1];
			SOURCE_FILE = args[2];
			TARGET_FILE = args[3];
			break;
		default:
			System.out.println("error - Invalid number of arguments; Program will now halt.");
			System.exit(-1);
		}

		System.out.println("info - Starting simulation");
		doMCDwalk(); // simulation execution
	}

	public static void doMCDwalk() {
		System.out.println("info - Input parameters done!");
		DirectedGraph<BioVertex, BioEdge> g = NetIO.inputDirectedGraphSif(NET_FILE);
		System.out.println("info - Input network done!");
		origNetEdges = g.getEdgeCount();
		origNetVertices = g.getVertexCount();

		if (!SOURCE_FILE.equals("") && !TARGET_FILE.equals("")) { // source and target files were provided
			NetIO.inputSpecifiedVertices(g, SOURCE_FILE, SOURCE_STATUS);
			NetIO.inputSpecifiedVertices(g, TARGET_FILE, TARGET_STATUS);
		}
		else if(!SOURCE_FILE.equals("") && TARGET_FILE.equals("")) { // will select random vertices with number provided in source
			NetUtil.setRandomSpecifiedVertices(g, Integer.parseInt(SOURCE_FILE));
			System.out.println("info - No input file for source/target nodes, will set random vertices as source/target");
		}
		else {
			System.out.println("error - Input vertices file names incorrectly set; Program will now halt");
			System.exit(-1);
		}

		System.out.println("info - Specified vertices are set");
		System.out.println("info - Output degree distribution of original graph");
		NetIO.outputDegreeDistribution(g, NetUtil.DegreeStatus.IN, NET_FILE.substring(0, NET_FILE.lastIndexOf(".")) + "_original.sif");
		NetIO.outputDegreeDistribution(g, NetUtil.DegreeStatus.OUT, NET_FILE.substring(0, NET_FILE.lastIndexOf(".")) + "_original.sif");
		NetIO.outputDegreeDistribution(g, NetUtil.DegreeStatus.INOUT, NET_FILE.substring(0, NET_FILE.lastIndexOf(".")) + "_original.sif");
		System.out.println("info - Pruning network (removing unreachable vertices)");
		NetSim.filterVertices(g, SOURCE_STATUS, TARGET_STATUS);
		System.out.println("info - Network pruning finished");
		System.out.println("info - Output degree distribution of pruned graph");
		NetIO.outputDegreeDistribution(g, NetUtil.DegreeStatus.IN, NET_FILE.substring(0, NET_FILE.lastIndexOf(".")) + "_pruned.sif");
		NetIO.outputDegreeDistribution(g, NetUtil.DegreeStatus.OUT, NET_FILE.substring(0, NET_FILE.lastIndexOf(".")) + "_pruned.sif");
		NetIO.outputDegreeDistribution(g, NetUtil.DegreeStatus.INOUT, NET_FILE.substring(0, NET_FILE.lastIndexOf(".")) + "_pruned.sif");
		System.out.println("info - Starting MCWalk...");
		NetSim.mcWalkSubnet(g, RUNS, STEPS, SOURCE_STATUS, TARGET_STATUS);
		System.out.println("info - MCWalk finished!");

		Set<org.jgrapht.GraphPath<BioVertex, BioEdge>> kPaths = null;
		Set<List<BioEdge>> shPaths = null;
		if (MAX_PATHS > 0 && MAX_HOPS > 0) {
			System.out.println("info - Starting to find k-shortest paths");
			kPaths = NetSim.findKShortestPaths(g, MAX_PATHS, MAX_HOPS, SOURCE_STATUS, TARGET_STATUS); // get all K-shortest paths
		}
		else if (MAX_PATHS == 0 && MAX_HOPS == 0) {
			System.out.println("info - Starting to find shortest paths");
			shPaths = NetSim.findShortestPaths(g, SOURCE_STATUS, TARGET_STATUS); // get all shortest paths
		}
		else {
			System.out.println("info - Will not calculate shortest paths");
		}

		if (kPaths != null) NetSim.getFilteredVerticesByPath(kPaths, 0); // if k-shortest paths were provided calculate & update vertex path scores

		System.out.println("info - Starting score output");

		// mark shortest path as score=1.0 vertices for subnetwork output
		if (shPaths != null && MARK_SHORTEST) {
			for (List<BioEdge> path : shPaths) {
				for (BioEdge e : path) {
					g.getEndpoints(e).getFirst().setScore(1.0);
					g.getEndpoints(e).getSecond().setScore(1.0);
					e.setScore(1.0);
				}
			}
		}

        NetSim.outputVerticesStats(g, NET_FILE);

		System.out.println("info - Starting subnetwork output");
		NetSim.outputSubnetworks(g, NET_FILE, THRES_MIN, THRES_MAX, THRES_STEP, FILTER_METHOD, kPaths);
		System.out.println("info - Simulation finished!");
	}

	private static void inputParameters(String parameterFile) {
		try {
			BufferedReader myReader = new BufferedReader(new FileReader(parameterFile));
			RUNS = (long)Double.parseDouble(nextReaderLine(myReader));
			STEPS = Integer.parseInt(nextReaderLine(myReader));
			THRES_MIN = Double.parseDouble(nextReaderLine(myReader));
			THRES_MAX = Double.parseDouble(nextReaderLine(myReader));
			THRES_STEP = Double.parseDouble(nextReaderLine(myReader));
			myReader.readLine();
			MAX_PATHS = Integer.parseInt(nextReaderLine(myReader));
			MAX_HOPS = Integer.parseInt(nextReaderLine(myReader));
			MARK_SHORTEST = Boolean.parseBoolean(nextReaderLine(myReader));
            PRUNE_AFTER = Boolean.parseBoolean(nextReaderLine(myReader));
			myReader.readLine();
			NET_FILE = nextReaderLine(myReader);
			SOURCE_FILE = nextReaderLine(myReader);
			TARGET_FILE = nextReaderLine(myReader);

			myReader.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.out.println("error - Parameters file not found; Program will now halt");
			System.exit(-1);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// helper function to read parameters
	private static String nextReaderLine(BufferedReader myReader) {
		String s = null;
		try {
			s = myReader.readLine();
		} catch (IOException e) {
			System.out.println("No line to read");
			e.printStackTrace();
		}
        if (s.contains("#")){
            return s.substring(s.indexOf("=") + 1, s.indexOf("#") - 1);
        }
        else {
            return s.substring(s.indexOf("=") + 1, s.length());
        }

	}

}