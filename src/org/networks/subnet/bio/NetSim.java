package org.networks.subnet.bio;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import edu.uci.ics.jung.algorithms.metrics.Metrics;
import edu.uci.ics.jung.algorithms.shortestpath.DijkstraDistance;
import edu.uci.ics.jung.algorithms.shortestpath.DijkstraShortestPath;
import edu.uci.ics.jung.graph.DirectedGraph;

import org.networks.subnet.bio.BioVertex.BioVertexStatus;
import org.networks.subnet.math.DependencyMatrixEntity;

public class NetSim {
	// Non instantiable class, contains functions for network simulations
	private NetSim() {
		throw new AssertionError();
	}

	// assigns a score to nodes based on random walks between seed nodes, to create a subnetwork
	// calculates source/target dependency matrix
	// every node traversed is considered ONCE in each walk
	public static void mcWalkSubnet(DirectedGraph<BioVertex, BioEdge> g, long runs, int steps, BioVertexStatus srcStatus, BioVertexStatus tgtStatus) {
		List<BioVertex> srcVertices = NetUtil.getSpecifiedVertices(g, srcStatus);
		List<BioVertex> tgtVertices = NetUtil.getSpecifiedVertices(g, tgtStatus);
		Collections.sort(srcVertices, new NetUtil.vertexNameComparator()); // sort initial source vertex list by name
		Collections.sort(tgtVertices, new NetUtil.vertexNameComparator()); // sort initial target vertex list by name
		DependencyMatrixEntity[][] dm = new DependencyMatrixEntity[srcVertices.size()][tgtVertices.size()]; // dependency matrix for source/targets, rows are sources & columns are targets
		DependencyMatrixEntity[][] dmScrFreq = new DependencyMatrixEntity[srcVertices.size()][tgtVertices.size()]; // sorted dependency matrix (rows: Score, columns: Frequency)
		DependencyMatrixEntity[][] dmScrExp = new DependencyMatrixEntity[srcVertices.size()][tgtVertices.size()]; // sorted dependency matrix (rows: Score, columns: Expression)
		double[] srcScoreFreq = new double[srcVertices.size()];
		double[] srcScoreExp = new double[srcVertices.size()];
		double[] tgtScoreFreq = new double[tgtVertices.size()];
		double[] tgtScoreExp = new double[tgtVertices.size()];

		long actualRuns = 0; // number of successful runs

        for (int i = 0; i < dm.length; i++) { // initialize dependency matrix arrays
            for (int j = 0; j < dm[i].length; j++) {
            	dm[i][j] = new DependencyMatrixEntity();
            	dmScrFreq[i][j] = new DependencyMatrixEntity();
            	dmScrExp[i][j] = new DependencyMatrixEntity();
            }
        }

		// get source vertices that reach to at least one target. Pruning was done so reachability has already been discovered
		List<BioVertex> srcReachingVertices = NetUtil.findReachingVertices(g);
		if (srcReachingVertices.isEmpty()) {
			System.out.println("error - Source nodes cannot reach any target in the network");
			System.exit(-1);
		}

		for (BioVertex v : g.getVertices()) { // ensure each node in the network has an initial score of 0
			v.setScore(0.0);
		}

		for (long i = 1L; i <= runs; i++) { // RUNS loop
			Set<BioVertex> sv = new HashSet<BioVertex>(); // set to keep score for current run
			Set<BioEdge> se = new HashSet<BioEdge>(); // set to keep score for current run

			BioVertex v = NetUtil.selectRandomVertex(srcReachingVertices); // begin random walk, select an initial random source vertex
			BioVertex srcOrigVertex = v; // keep track of original source vertex
			boolean walkSuccess = false; // is the walk successful?

			if ((i >= 10 && i < 100 && i % 10 == 0) || (i >= 100 && i < 1000 && i % 100 == 0) || (i >= 1000 && i < 10000 && i % 1000 == 0) ||
					(i >= 10000 && i < 100000 && i % 10000 == 0) || (i >= 100000 && i < 1000000 && i % 100000 == 0) ||
					(i >= 1000000L && i < 10000000L && i % 1000000L == 0) || (i >= 10000000L && i < 100000000L && i % 10000000L == 0) || (i >= 100000000L && i <= 1000000000L && i % 100000000L == 0) ||
					(i >= 1000000000L && i < 10000000000L && i % 1000000000L == 0) || (i >= 10000000000L && i < 100000000000L && i % 10000000000L == 0) ||
					(i >= 100000000000L && i <= 1000000000000L && i % 100000000000L == 0)) { // output information of program running in console
				System.out.println("info - Starting run: " + i + ", ActualRuns up to now: " + actualRuns);
			}

			for (int j = 1; j <= steps; j++) { // MCS loop, ensure walk ends after a specific number of steps
				if (g.getSuccessorCount(v) > 0) { // v has successors
					BioVertex o = v; // old vertex
					List<BioVertex> n = new ArrayList<BioVertex>(g.getSuccessors(v)); // get adjacent successors
					v = n.get(NetUtil.rand.nextIntFromTo(0, n.size() - 1)); // walker moves, vertex is a random successor node

					se.add(g.findEdge(o, v)); // keeps edge score

					if (v.getStatus().contains(tgtStatus) && v != srcOrigVertex) { // we have arrived at a target node, NOT-INCLUDING starting node
						walkSuccess = true;
						break;
					}
					else {
						sv.add(v); // add to set for keeping score
						// v.setScore(v.getScore() + 1.0);
					}
				}
				else { // v has no successors, end MCS
					break;
				}
			}

			if (walkSuccess) { // the walk was successful, update node scores from corresponding set
				int srcIndex = srcVertices.indexOf(srcOrigVertex); int tgtIndex = tgtVertices.indexOf(v);

				for (BioVertex v2 : sv) { //update vertices
					v2.setScore(v2.getScore() + 1.0);
				}
				for (BioEdge e2 : se) { //update edges
					e2.setScore(e2.getScore() + 1.0);
				}
				dm[srcIndex][tgtIndex].probability++; // increase corresponding matrix element, rows are sources columns are targets
				actualRuns++; // increase number of successful runs
			}
		} // end runs

		for (BioVertex v : g.getVertices()) {  // normalize vertex score
			if (v.getStatus().contains(srcStatus) || v.getStatus().contains(tgtStatus)) {
				v.setScore(1.0); // set source, target nodes to score = 1.0
			}
			else {
				v.setScore(v.getScore() / actualRuns);
			}
		}

		for (BioEdge e : g.getEdges()) { // normalize edge score
			e.setScore(e.getScore() / actualRuns);
		}

		try {
			NumberFormat fmt9d = new DecimalFormat("0.#########");

			DijkstraDistance<BioVertex, BioEdge> dist = new DijkstraDistance<BioVertex, BioEdge>(g, false); // distance object

			 // initialize and normalize dependency matrix, include distances
			for (int i = 0; i < dm.length; i++) {
	        	double sum = 0.0;
	            for (int j = 0; j < dm[i].length; j++) {
	            	if (i==j) {
	            		dm[i][j].probability = 0.0; // diagonal set to 0.0
	            	}
	            	else {
		            	dm[i][j].probability /= actualRuns; // normalize with actualRuns
		            	sum += dm[i][j].probability;
	            	}
	            }
	            for (int j = 0; j < dm[i].length; j++) {
	            	if (dm[i][j].probability > 0.0 && dm[i][j].probability < 1.0 && sum > 0.0) {
	            		dm[i][j].probability /= sum; // normalize with total row sum
	            	}
	            }
	            for (int j = 0; j < dm[i].length; j++) { // set corresponding values
	            	dm[i][j].sourceVertex = srcVertices.get(i);
	            	dm[i][j].targetVertex = tgtVertices.get(j);

	            	 // calculate distances
	            	if (i != j) {
	            		Number d = dist.getDistance(dm[i][j].sourceVertex, dm[i][j].targetVertex);
	            		if (d == null) {
	            			dm[i][j].distance = 0;
	            		}
	            		else {
		            		dm[i][j].distance = d.intValue();
	            		}
	            	}
	            }
	        }

	        // calculate and reorder dependency matrix
	        for (int i = 0; i < dmScrFreq.length; i++) { // update name and value of sorted matrices
	            for (int j = 0; j < dmScrFreq[i].length; j++) {
	            	dmScrFreq[i][j].probability = dm[i][j].probability;
	            	dmScrFreq[i][j].sourceVertex = dm[i][j].sourceVertex;
	            	dmScrFreq[i][j].targetVertex = dm[i][j].targetVertex;

	            	dmScrExp[i][j].probability = dm[i][j].probability;
	            	dmScrExp[i][j].sourceVertex = dm[i][j].sourceVertex;
	            	dmScrExp[i][j].targetVertex = dm[i][j].targetVertex;
	            }
	        }

	        for (int i = 0; i < dm.length; i++) { // calculate total score of targets (frequency, expression)
	            for (int j = 0; j < dm[i].length; j++) {
	            	tgtScoreFreq[j] += dm[i][j].probability;
	            	tgtScoreExp[j] = Math.abs(dm[i][j].targetVertex.getExpLevel()); // absolute expression is score
	            }
	        }

	        for (int i = 0; i < dm.length; i++) { // calculate total score of sources based on targets (frequency, expression)
	            for (int j = 0; j < dm[i].length; j++) {
	            	srcScoreFreq[i] += (dm[i][j].probability * tgtScoreFreq[j]);
	            	srcScoreExp[i] += (dm[i][j].probability * tgtScoreExp[j]);
	            }
	        }

	        for (int i = 0; i < dmScrFreq.length; i++) { // update source/target scores in sorted dependency matrices
	            for (int j = 0; j < dmScrFreq[i].length; j++) { // scores are frequencies and expression levels in corresponding matrices
	            	dmScrFreq[i][j].targetScore = tgtScoreFreq[j];
	            	dmScrFreq[i][j].sourceScore = srcScoreFreq[i];
	            	dmScrExp[i][j].targetScore = tgtScoreExp[j];
	            	dmScrExp[i][j].sourceScore = srcScoreExp[i];
	            }
	        }

	        for (int i = 0; i < dmScrFreq.length; i++) {  // reorder columns (target frequency, target expression)
				Arrays.sort(dmScrFreq[i], Collections.reverseOrder(new NetUtil.targetScoreComparator()));
				Arrays.sort(dmScrExp[i], Collections.reverseOrder(new NetUtil.targetScoreComparator()));
			}

	        Arrays.sort(dmScrFreq, Collections.reverseOrder(new NetUtil.sourceScoreComparator())); // reorder rows (source score based on frequency)
	        Arrays.sort(dmScrExp, Collections.reverseOrder(new NetUtil.sourceScoreComparator())); // reorder rows (source score based on expression)

	        // output dependency matrix
			BufferedWriter wDep = new BufferedWriter(new FileWriter(Program.NET_FILE.substring(0, Program.NET_FILE.lastIndexOf(".")) + "_depMatrix.txt"));
			BufferedWriter wDepScrFreq = new BufferedWriter(new FileWriter(Program.NET_FILE.substring(0, Program.NET_FILE.lastIndexOf(".")) + "_depMatrixSorted-Freq.txt"));
			BufferedWriter wDepScrExp = new BufferedWriter(new FileWriter(Program.NET_FILE.substring(0, Program.NET_FILE.lastIndexOf(".")) + "_depMatrixSorted-Exp.txt"));
			BufferedWriter wSrcScoreFreq = new BufferedWriter(new FileWriter(Program.NET_FILE.substring(0, Program.NET_FILE.lastIndexOf(".")) + "_srcScoresFreq.txt"));
			BufferedWriter wTgtScoreFreq = new BufferedWriter(new FileWriter(Program.NET_FILE.substring(0, Program.NET_FILE.lastIndexOf(".")) + "_tgtScoresFreq.txt"));
			BufferedWriter wSrcScoreExp = new BufferedWriter(new FileWriter(Program.NET_FILE.substring(0, Program.NET_FILE.lastIndexOf(".")) + "_srcScoresExp.txt"));
			BufferedWriter wTgtScoreExp = new BufferedWriter(new FileWriter(Program.NET_FILE.substring(0, Program.NET_FILE.lastIndexOf(".")) + "_tgtScoresExp.txt"));

			for (int i = 0; i < dm[0].length; i++) { // write columns (targets)
				wDep.write("\t" + dm[0][i].targetVertex.getName());
				wDepScrFreq.write("\t" + dmScrFreq[0][i].targetVertex.getName());
				wDepScrExp.write("\t" + dmScrExp[0][i].targetVertex.getName());
			}
			wDep.newLine(); wDepScrFreq.newLine(); wDepScrExp.newLine();

	        for (int i = 0; i < dm.length; i++) {
	        	wDep.write(dm[i][0].sourceVertex.getName());
	        	wDepScrFreq.write(dmScrFreq[i][0].sourceVertex.getName());
	        	wDepScrExp.write(dmScrExp[i][0].sourceVertex.getName());
	            for (int j = 0; j < dm[i].length; j++) {
	            	wDep.write("\t" + fmt9d.format(dm[i][j].probability));
	            	wDepScrFreq.write("\t" + fmt9d.format(dmScrFreq[i][j].probability));
	            	wDepScrExp.write("\t" + fmt9d.format(dmScrExp[i][j].probability));
	            }
	            // expression levels for each source node
            	wDep.write("\t" + fmt9d.format(dm[i][0].sourceVertex.getExpLevel()));
            	wDepScrFreq.write("\t" + fmt9d.format(dmScrFreq[i][0].sourceVertex.getExpLevel()));
            	wDepScrExp.write("\t" + fmt9d.format(dmScrExp[i][0].sourceVertex.getExpLevel()));

				wDep.newLine(); wDepScrFreq.newLine(); wDepScrExp.newLine();
	        }

	        // expression levels for each target node
            for (int j = 0; j < dm[0].length; j++) {
            	wDep.write("\t" + fmt9d.format(dm[0][j].targetVertex.getExpLevel()));
            	wDepScrFreq.write("\t" + fmt9d.format(dmScrFreq[0][j].targetVertex.getExpLevel()));
            	wDepScrExp.write("\t" + fmt9d.format(dmScrExp[0][j].targetVertex.getExpLevel()));
            }


	        // output source frequency scores
	        for (int i = 0; i < dmScrFreq.length; i++) {
				wSrcScoreFreq.write(dmScrFreq[i][0].sourceVertex.getName() + "\t" + fmt9d.format(dmScrFreq[i][0].sourceScore));
				wSrcScoreFreq.newLine();
			}

	        // output target frequency scores
	        for (int j = 0; j < dmScrFreq[0].length; j++) {
				wTgtScoreFreq.write(dmScrFreq[0][j].targetVertex.getName() + "\t" + fmt9d.format(dmScrFreq[0][j].targetScore));
				wTgtScoreFreq.newLine();
			}

	        // output source expression scores
	        for (int i = 0; i < dmScrExp.length; i++) {
				wSrcScoreExp.write(dmScrExp[i][0].sourceVertex.getName() + "\t" + fmt9d.format(dmScrExp[i][0].sourceScore));
				wSrcScoreExp.newLine();
			}

	        // output target expression scores
	        for (int j = 0; j < dmScrExp[0].length; j++) {
				wTgtScoreExp.write(dmScrExp[0][j].targetVertex.getName() + "\t" + fmt9d.format(dmScrExp[0][j].targetScore));
				wTgtScoreExp.newLine();
			}

	        wDep.close(); wDepScrFreq.close(); wDepScrExp.close();
	        wSrcScoreFreq.close(); wTgtScoreFreq.close(); wSrcScoreExp.close(); wTgtScoreExp.close();

	        // output dependency matrix as network
	        DirectedGraph<BioVertex,BioEdge> pGraph = NetUtil.convertDependencyMatrix(dm, "probability");
	        DirectedGraph<BioVertex,BioEdge> dGraph = NetUtil.convertDependencyMatrix(dm, "distance");
	        NetIO.outputDirectedGraphSif(pGraph, Program.NET_FILE.substring(0, Program.NET_FILE.lastIndexOf(".")) + "_depNetwork.sif");
	        NetIO.outputEdgeAttributeWeights(pGraph, Program.NET_FILE.substring(0, Program.NET_FILE.lastIndexOf(".")) + "_depNetEdgeAttrW.NA");
	        NetIO.outputDirectedGraphSif(dGraph, Program.NET_FILE.substring(0, Program.NET_FILE.lastIndexOf(".")) + "_distNetwork.sif");
	        NetIO.outputEdgeAttributeWeights(dGraph, Program.NET_FILE.substring(0, Program.NET_FILE.lastIndexOf(".")) + "_distNetEdgeAttrD.NA");

		} catch (IOException e) {
			e.printStackTrace();
		}

		Program.ACTUAL_RUNS = actualRuns; // update actual runs
	} // end mcwalk

	// removes vertices with score below the specified threshold, keeps vertices with status srcStatus or tgtStatus
	// *** MODIFIES ORIGINAL GRAPH ***
	public static void filterVertices(DirectedGraph<BioVertex, BioEdge> g, double threshold, BioVertexStatus srcStatus, BioVertexStatus tgtStatus) {
		List<BioVertex> lv = new ArrayList<BioVertex>(g.getVertices());

		for (BioVertex v : lv) {
			if (threshold - v.getScore() > 1E-12 && !(v.getStatus().contains(srcStatus) || v.getStatus().contains(tgtStatus))) {
				g.removeVertex(v);
			}
		}

		lv = new ArrayList<BioVertex>(g.getVertices());
		for (BioVertex v : lv) {
			if (g.degree(v) == 0) {
				g.removeVertex(v);
			}
		}
	}

	// removes vertices that don't belong to the provided set, keeps vertices with status srcStatus or tgtStatus
	// *** MODIFIES ORIGINAL GRAPH ***
	public static void filterVertices(DirectedGraph<BioVertex, BioEdge> g, Set<BioVertex> vs, BioVertexStatus srcStatus, BioVertexStatus tgtStatus) {
		List<BioVertex> lv = new ArrayList<BioVertex>(g.getVertices());

		for (BioVertex v : lv) {
			if (!vs.contains(v) && !(v.getStatus().contains(srcStatus) || v.getStatus().contains(tgtStatus))) {
				g.removeVertex(v);
			}
		}

		lv = new ArrayList<BioVertex>(g.getVertices());
		for (BioVertex v : lv) {
			if (g.degree(v) == 0) {
				g.removeVertex(v);
			}
		}
	}

	// removes all unreachable vertices (keeps vertices that are reachable from at least one source and reach at least one target)
	// *** MODIFIES ORIGINAL GRAPH ***
	public static void filterVertices(DirectedGraph<BioVertex, BioEdge> g, BioVertexStatus srcStatus, BioVertexStatus tgtStatus) {
		NetUtil.findVertexReachability(g, srcStatus, tgtStatus);
		List<BioVertex> normVertices = NetUtil.getSpecifiedVertices(g, BioVertexStatus.NORMAL);

		for(BioVertex v : normVertices) { // remove non-traversible NORMAL vertices
			if (!v.isTraversible()) {
				g.removeVertex(v);
			}
		}
	}


	// removes edges with score below the specified threshold
	// *** MODIFIES ORIGINAL GRAPH ***
	public static void filterEdges(DirectedGraph<BioVertex, BioEdge> g, double threshold) {
		List<BioVertex> lv = new ArrayList<BioVertex>(g.getVertices());
		List<BioEdge> le = new ArrayList<BioEdge>(g.getEdges());

		for (BioEdge e : le) {
			if (threshold - e.getScore() > 1E-12) {
				g.removeEdge(e);
			}
		}

		lv = new ArrayList<BioVertex>(g.getVertices());
		for (BioVertex v : lv) {
			if (g.degree(v) == 0) {
				g.removeVertex(v);
			}
		}
	}

	// get k-shortest paths between specified nodes, output matrices for i) distances and ii) number of paths between source/target nodes
	public static Set<org.jgrapht.GraphPath<BioVertex, BioEdge>> findKShortestPaths(DirectedGraph<BioVertex, BioEdge> g, int paths, int maxHops, BioVertexStatus srcStatus, BioVertexStatus tgtStatus) {
		org.jgrapht.DirectedGraph<BioVertex, BioEdge> jg = NetUtil.convertDirectedGraph(g);
		Set<org.jgrapht.GraphPath<BioVertex, BioEdge>> sgp = new HashSet<org.jgrapht.GraphPath<BioVertex, BioEdge>>();

		try {
			BufferedWriter wTargets = new BufferedWriter(new FileWriter(Program.NET_FILE.substring(0, Program.NET_FILE.lastIndexOf(".")) + "_tgts.txt"));
			BufferedWriter wDist = new BufferedWriter(new FileWriter(Program.NET_FILE.substring(0, Program.NET_FILE.lastIndexOf(".")) + "_distMatrix.txt"));
			BufferedWriter wPaths = new BufferedWriter(new FileWriter(Program.NET_FILE.substring(0, Program.NET_FILE.lastIndexOf(".")) + "_tgtpaths.txt"));
			wTargets.write("Source\tTargets");

			List<BioVertex> srcVertices = NetUtil.getSpecifiedVertices(jg, srcStatus);
			List<BioVertex> tgtVertices = NetUtil.getSpecifiedVertices(jg, tgtStatus);
			Collections.sort(srcVertices, new NetUtil.vertexNameComparator());
			Collections.sort(tgtVertices, new NetUtil.vertexNameComparator());

			for (BioVertex v : tgtVertices) { // write target names (columns)
				wDist.write("\t" + v.getName());
				wPaths.write("\t" + v.getName());
			}

			DijkstraDistance<BioVertex, BioEdge> dist = new DijkstraDistance<BioVertex, BioEdge>(g, false);

			for (BioVertex vs : srcVertices) {
				wTargets.newLine(); wTargets.write(vs.getName());
				wDist.newLine(); wDist.write(vs.getName());
				wPaths.newLine(); wPaths.write(vs.getName());

				org.jgrapht.alg.KShortestPaths<BioVertex, BioEdge> k = new org.jgrapht.alg.KShortestPaths<BioVertex, BioEdge>(jg, vs, paths, maxHops);

				for (BioVertex vt : tgtVertices) {
					if (vs != vt) {
						Number d = dist.getDistance(vs, vt); // write distance
						if (d == null) { // vertex unreachable (distance == 0)
							wDist.write("\t" + 0);
						}
						else {
							wDist.write("\t" + d.intValue());
						}

						List<org.jgrapht.GraphPath<BioVertex, BioEdge>> gpl = k.getPaths(vt);

						if(gpl == null) { //  no paths exist
							wPaths.write("\t" + 0);
						}
						else { // there is at least one path
							wTargets.write("\t" + vt.getName());
							wPaths.write("\t" + gpl.size());

							for (org.jgrapht.GraphPath<BioVertex, BioEdge> gp : gpl) { // get all paths from vs to vt
								sgp.add(gp);
							}
						}
					}
					else { // source and vertex are the same
						wDist.write("\t" + 0);
					}
				}
			}

			wTargets.close();
			wDist.close();
			wPaths.close();
		} catch (NullPointerException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return sgp;
	}

	// get shortest paths between specified nodes, output distance matrix
	public static Set<List<BioEdge>> findShortestPaths(DirectedGraph<BioVertex, BioEdge> g, BioVertexStatus srcStatus, BioVertexStatus tgtStatus) {
		Set<List<BioEdge>> sgp = new HashSet<List<BioEdge>>();

		try {
			BufferedWriter wTargets = new BufferedWriter(new FileWriter(Program.NET_FILE.substring(0, Program.NET_FILE.lastIndexOf(".")) + "_tgts.txt"));
			BufferedWriter wDist = new BufferedWriter(new FileWriter(Program.NET_FILE.substring(0, Program.NET_FILE.lastIndexOf(".")) + "_distMatrix.txt"));
			wTargets.write("Source\tTargets");

			List<BioVertex> srcVertices = NetUtil.getSpecifiedVertices(g, srcStatus);
			List<BioVertex> tgtVertices = NetUtil.getSpecifiedVertices(g, tgtStatus);
			Collections.sort(srcVertices, new NetUtil.vertexNameComparator());
			Collections.sort(tgtVertices, new NetUtil.vertexNameComparator());

			for (BioVertex v : tgtVertices) { // write target names (columns)
				wDist.write("\t" + v.getName());
			}

			DijkstraShortestPath<BioVertex, BioEdge> dpath = new DijkstraShortestPath<BioVertex, BioEdge>(g, false);

			for (BioVertex vs : srcVertices) {
				wTargets.newLine(); wTargets.write(vs.getName());
				wDist.newLine(); wDist.write(vs.getName());

				for (BioVertex vt : tgtVertices) {
					if (vs != vt) {

						Number d = dpath.getDistance(vs, vt); // write distance

						if (d == null) { // vertex unreachable (distance == 0)
							wDist.write("\t" + 0);
						}
						else {
							wDist.write("\t" + d.intValue());
						}

						List<BioEdge> gp = dpath.getPath(vs, vt);

						if(gp != null) { // there is at least one path
							wTargets.write("\t" + vt.getName());
							sgp.add(gp);
						}
					}
					else { // source and vertex are the same
						wDist.write("\t" + 0);
					}
				}
			}

			wTargets.close();
			wDist.close();
		} catch (NullPointerException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return sgp;
	}

	// outputs a set of subnetworks for various threshold values
	public static void outputSubnetworks(DirectedGraph<BioVertex, BioEdge> g, String netFileName, double thr_start, double thr_end,  double thresStep, FilterMethod fMethod, Set<org.jgrapht.GraphPath<BioVertex, BioEdge>> paths) {
		try {
			String origNamePrefix = netFileName.substring(netFileName.lastIndexOf("/") + 1, netFileName.lastIndexOf("."));// just get filename without extension
			String netFilePrefix = netFileName.substring(0, netFileName.lastIndexOf("/")) + "/networks/";
			File dir = new File(netFilePrefix);
			dir.mkdir();
			netFilePrefix += origNamePrefix;
			DirectedGraph<BioVertex, BioEdge> gv = NetUtil.copyDirectedGraph(g);
			DirectedGraph<BioVertex, BioEdge> ge = NetUtil.copyDirectedGraph(g);
			List<BioVertex> srcVerticesG = NetUtil.getSpecifiedVertices(g, Program.SOURCE_STATUS);
			List<BioVertex> tgtVerticesG = NetUtil.getSpecifiedVertices(g, Program.TARGET_STATUS);

			BufferedWriter vWriter = new BufferedWriter(new FileWriter(netFileName.substring(0, netFileName.lastIndexOf(".")) + "_v-stats.txt"));
			BufferedWriter eWriter = new BufferedWriter(new FileWriter(netFileName.substring(0, netFileName.lastIndexOf(".")) + "_e-stats.txt"));

			vWriter.write("Network name: " + netFileName + "\tAlgorithm: mcwalk" + "\tSrc - " + Program.SOURCE_STATUS  + ": " +  srcVerticesG.size()
					+ "\tTgt - " + Program.TARGET_STATUS + ": " +  tgtVerticesG.size() + "\tFilter Method: " + fMethod); vWriter.newLine();
			vWriter.write("Runs: " + Program.RUNS + "\tActualRuns: " + Program.ACTUAL_RUNS + "\tSteps: " + Program.STEPS + "\tMaxPaths: " +
					Program.MAX_PATHS + "\tMaxHops: " + Program.MAX_HOPS); vWriter.newLine();
			vWriter.write("thres\tnodes\tedges" + "\tSrc - " + Program.SOURCE_STATUS + "\tTgt - " + Program.TARGET_STATUS + "\tconnected"); vWriter.newLine();
			eWriter.write("Network name: " + netFileName + "\tAlgorithm: mcwalk" + "\tSrc - " + Program.SOURCE_STATUS  + ": " +  srcVerticesG.size()
					+ "\tTgt - " + Program.TARGET_STATUS + ": " +  tgtVerticesG.size() + "\tFilter Method: " + fMethod); eWriter.newLine();
			eWriter.write("Runs: " + Program.RUNS + "\tActualRuns: " + Program.ACTUAL_RUNS + "\tSteps: " + Program.STEPS + "\tMaxPaths: " +
					Program.MAX_PATHS + "\tMaxHops: " + Program.MAX_HOPS); eWriter.newLine();
			eWriter.write("thres\tnodes\tedges" + "\tSrc - " + Program.SOURCE_STATUS + "\tTgt - " + Program.TARGET_STATUS + "\tconnected"); eWriter.newLine();

			NumberFormat fmt9d = new DecimalFormat("0.#########");

			Set<BioVertex> vsp;

			boolean nodesConGV = false; boolean nodesConGE = false;
			boolean nodesConG = NetUtil.checkSpecifiedNodesWeaklyConnected(g, Program.SOURCE_STATUS, Program.TARGET_STATUS);
			NetIO.outputDirectedGraphSif(g, netFilePrefix + "_t" + fmt9d.format(0) + ".sif"); // write unfiltered graph

			vWriter.write(fmt9d.format(0) + "\t" + Program.origNetVertices + "\t" + Program.origNetEdges); vWriter.newLine();
			vWriter.write(fmt9d.format(0) + "\t" + g.getVertexCount() + "\t" + g.getEdgeCount() + "\t" + srcVerticesG.size() +
					"\t" + tgtVerticesG.size() + "\t" + nodesConG); vWriter.newLine();

			eWriter.write(fmt9d.format(0) + "\t" + Program.origNetVertices + "\t" + Program.origNetEdges); eWriter.newLine();
			eWriter.write(fmt9d.format(0) + "\t" + g.getVertexCount() + "\t" + g.getEdgeCount() + "\t" + srcVerticesG.size() +
					"\t" + tgtVerticesG.size() + "\t" + nodesConG); eWriter.newLine();

			if (thresStep > 0.0) // output subnetworks for a specific threshold step from thr_start to thr_end
				for (double t = thr_start;  thr_end - t > 1E-12; t += thresStep) {
					switch (fMethod) {
					case NORMAL:
						filterVertices(gv, t, Program.SOURCE_STATUS, Program.TARGET_STATUS);
						filterEdges(ge, t);
						break;
					case BY_PATH: // experimental, use NORMAL filtering for regular analysis
						vsp = getFilteredVerticesByPath(paths, t); // gets filtered vertices by path (i.e. set of vertices that belong at least to one path with average more than the threshold)
						filterVertices(gv, vsp, Program.SOURCE_STATUS, Program.TARGET_STATUS);
						break;
					default:
						break;
					}

                    // remove unreachable vertices in a final pruning step
                    if (Program.PRUNE_AFTER) {
                        NetSim.filterVertices(gv, BioVertexStatus.SOURCE, BioVertexStatus.TARGET);
                        NetSim.filterVertices(ge, BioVertexStatus.SOURCE, BioVertexStatus.TARGET);
                    }

					nodesConGV = NetUtil.checkSpecifiedNodesWeaklyConnected(gv, Program.SOURCE_STATUS, Program.TARGET_STATUS);
					nodesConGE = NetUtil.checkSpecifiedNodesWeaklyConnected(ge, Program.SOURCE_STATUS, Program.TARGET_STATUS);

					NetIO.outputDirectedGraphSif(gv, netFilePrefix + "-v" + "_t" + fmt9d.format(t) + ".sif");
					NetIO.outputDirectedGraphSif(ge, netFilePrefix + "-e" + "_t" + fmt9d.format(t) + ".sif");
					vWriter.write(fmt9d.format(t) + "\t" + gv.getVertexCount() + "\t" + gv.getEdgeCount() + "\t" + NetUtil.getSpecifiedVertices(gv, Program.SOURCE_STATUS).size() +
							"\t" + NetUtil.getSpecifiedVertices(gv, Program.TARGET_STATUS).size() + "\t" + nodesConGV); vWriter.newLine();
					eWriter.write(fmt9d.format(t) + "\t" + ge.getVertexCount() + "\t" + ge.getEdgeCount() + "\t" + NetUtil.getSpecifiedVertices(ge, Program.SOURCE_STATUS).size() +
								"\t" + NetUtil.getSpecifiedVertices(ge, Program.TARGET_STATUS).size() + "\t" + nodesConGE); eWriter.newLine();
				}
			else {
				for (double d = thr_start;  thr_end - d > 1E-12; d *= 10) { // output 10 subnetworks for every order of magnitude
					for (double t = d; (d * 10) - t > 1E-12; t += d) {

						switch (fMethod) {
						case NORMAL:
							filterVertices(gv, t, Program.SOURCE_STATUS, Program.TARGET_STATUS);
							filterEdges(ge, t);
							break;
						case BY_PATH: // experimental, use NORMAL filtering for regular analysis
							vsp = getFilteredVerticesByPath(paths, t); // gets filtered vertices by path (i.e. set of vertices that belong at least to one path with average more than the threshold)
							filterVertices(gv, vsp, Program.SOURCE_STATUS, Program.TARGET_STATUS);
							break;
						default:
							break;
						}

                        // remove unreachable vertices in a final pruning step
                        if (Program.PRUNE_AFTER) {
                            NetSim.filterVertices(gv, BioVertexStatus.SOURCE, BioVertexStatus.TARGET);
                            NetSim.filterVertices(ge, BioVertexStatus.SOURCE, BioVertexStatus.TARGET);
                        }

						nodesConGV = NetUtil.checkSpecifiedNodesWeaklyConnected(gv, Program.SOURCE_STATUS, Program.TARGET_STATUS);
						nodesConGE = NetUtil.checkSpecifiedNodesWeaklyConnected(ge, Program.SOURCE_STATUS, Program.TARGET_STATUS);

						NetIO.outputDirectedGraphSif(gv, netFilePrefix + "-v" + "_t" + fmt9d.format(t) + ".sif");
						NetIO.outputDirectedGraphSif(ge, netFilePrefix + "-e" + "_t" + fmt9d.format(t) + ".sif");
						vWriter.write(fmt9d.format(t) + "\t" + gv.getVertexCount() + "\t" + gv.getEdgeCount() + "\t" + NetUtil.getSpecifiedVertices(gv, Program.SOURCE_STATUS).size() +
								"\t" + NetUtil.getSpecifiedVertices(gv, Program.TARGET_STATUS).size() + "\t" + nodesConGV); vWriter.newLine();
						eWriter.write(fmt9d.format(t) + "\t" + ge.getVertexCount() + "\t" + ge.getEdgeCount() + "\t" + NetUtil.getSpecifiedVertices(ge, Program.SOURCE_STATUS).size() +
									"\t" + NetUtil.getSpecifiedVertices(ge, Program.TARGET_STATUS).size() + "\t" + nodesConGE); eWriter.newLine();
					}
				}
			}

			vWriter.close();
			eWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NullPointerException e) {
			e.printStackTrace();
		}
	}

	// outputs vertex scores and other statistics
	public static void outputVerticesStats(DirectedGraph<BioVertex, BioEdge> g, String scoreFile) {
		NumberFormat fmt9d = new DecimalFormat("0.#########");
		Map<BioVertex, Double> met = Metrics.clusteringCoefficients(g);

		try {
			BufferedWriter wn = new BufferedWriter(new FileWriter(scoreFile.substring(0, scoreFile.lastIndexOf(".")) + "_vertexScores.txt"));
			BufferedWriter we = new BufferedWriter(new FileWriter(scoreFile.substring(0, scoreFile.lastIndexOf(".")) + "_edgeScores.txt"));
			wn.write("Name\tStatus\tVertexScore\tMaxPathScore\tinDegree\toutDegree\tDegree\tClustCoeff"); wn.newLine();
			we.write("Source\tInteraction\tTarget\tEdgeScore"); we.newLine();

			for (BioVertex v : g.getVertices()) {
				double vScore = v.getScore();
				double vPathScore = v.calcMaxPathScore();

				if (vScore > 1E-12 || vPathScore > 1E-12) {
					wn.write(v.getName() + "\t" + v.getStatus() + "\t" + fmt9d.format(vScore) + "\t" + fmt9d.format(vPathScore) +
							"\t" + g.inDegree(v) + "\t" + g.outDegree(v)+ "\t" + g.degree(v) + "\t" + fmt9d.format(met.get(v)));
					wn.newLine();
				}
			}
			for (BioEdge e : g.getEdges()) {
				double eScore = e.getScore();

				if (eScore > 1E-12) {
					we.write(g.getSource(e).getName() + "\t" + e.getName() + "\t" + g.getDest(e).getName() + "\t" + fmt9d.format(eScore));
					we.newLine();
				}
			}

			wn.close();
			we.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// finds paths with average score above the specified threshold
	// returns a set of nodes which belong in at least one path with average above the threshold
	// updates the path score list of each node
	// *** DOESN'T MODIFY ORIGINAL GRAPH ***
    @Deprecated
	public static Set<BioVertex> getFilteredVerticesByPath(Set<org.jgrapht.GraphPath<BioVertex, BioEdge>> gpSet, double threshold) {
		Set<BioVertex> s = new HashSet<BioVertex>();

		for (org.jgrapht.GraphPath<BioVertex, BioEdge> gp : gpSet) { // nodes in provided set of paths
			List<BioVertex> lp = org.jgrapht.Graphs.getPathVertexList(gp);
			double avPathScore = 0.0;
			int n = 0;
			for (BioVertex vp : lp) { // calculate average path score
				if (vp.getStatus().contains(BioVertexStatus.NORMAL))
				avPathScore += vp.getScore();
				n++;
			}
			avPathScore /= n;

			if (avPathScore > threshold) { // add to set if path score exceeds threshold
				s.addAll(lp);
			}

			if (!BioVertex.getAvPathsCalculated()) { // if average scores for paths per vertex have not been calculated (to calculate only ONCE)
				for (BioVertex vp : lp) { // update average path score for each vertex
					if (vp.getStatus().contains(BioVertexStatus.NORMAL)){
						vp.getAvPathScores().add(avPathScore);
					}
				}
			}
		}
		BioVertex.setAvPathsCalculated(true);

		return s;
	}

	// normalize node according to max score, seed nodes set to score = 1.0
	@Deprecated
	private static void normalizeMaxScore(DirectedGraph<BioVertex, BioEdge> g) {
		double max = 0.0;

		for (BioVertex v : g.getVertices()) { // find maximum score
			if (max < v.getScore()) max = v.getScore();
		}

		for (BioVertex v : g.getVertices()) { // normalize node for maximum score
			if (!v.getStatus().contains(BioVertexStatus.NORMAL)) v.setScore(1.0);
			else v.setScore(v.getScore() / max);
		}
	}

	// get a set of BioPaths given a set of GraphPaths
	@Deprecated
	private static Set<BioPath> getBioPaths(Set<org.jgrapht.GraphPath<BioVertex, BioEdge>> gpSet) {
		Set<BioPath> bp = new HashSet<BioPath>();
		for (org.jgrapht.GraphPath<BioVertex, BioEdge> gp : gpSet) {
			bp.add(new BioPath(gp, true));
		}

		return bp;
	}

	public enum FilterMethod {
		NORMAL, BY_PATH
	}
}
