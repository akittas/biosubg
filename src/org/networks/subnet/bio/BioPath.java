package org.networks.subnet.bio;

import org.jgrapht.GraphPath;
import org.jgrapht.Graphs;

public class BioPath {
	private GraphPath<BioVertex, BioEdge> path;
	private double avg_score;
	private double stdev_score;

	public BioPath(GraphPath<BioVertex, BioEdge> pathValue, boolean calcScores) {
		path = pathValue;
		if (calcScores) {
			calcAvgVertexScore();
			calcStdevScore();
		}
		else {
			avg_score = 0.0;
			stdev_score = 0.0;
		}
	}

	public GraphPath<BioVertex, BioEdge> getPath() {
		return path;
	}

	public double getAvScore() {
		return avg_score;
	}

	public double getStdev_score() {
		return stdev_score;
	}

	// calculates the average score of the vertices in the path
	public double calcAvgVertexScore() {
		double sc = 0.0;
		for (BioVertex v : Graphs.getPathVertexList(path)) {
			sc += v.getScore();
		}
		sc /= Graphs.getPathVertexList(path).size();

		return sc;
	}

	// calculate the standard deviation of the path
	public double calcStdevScore() {
		double av = calcAvgVertexScore();
		double sd = 0.0;

		for (BioVertex v : Graphs.getPathVertexList(path)) {
			sd += Math.pow(v.getScore() - av, 2.0);
		}
		sd = Math.sqrt(sd / Graphs.getPathVertexList(path).size());

		return sd;
	}
}