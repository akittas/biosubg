package org.networks.subnet.bio;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.LineNumberReader;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.Locale;

import org.networks.subnet.bio.BioVertex.BioVertexStatus;

import edu.uci.ics.jung.graph.DirectedGraph;
import edu.uci.ics.jung.graph.DirectedSparseGraph;
import edu.uci.ics.jung.graph.Graph;


public class NetIO {
	// Non-instantiable utility class
	private NetIO() {
		throw new AssertionError();
	}

	// creates a directed graph from a sif format file
	public static DirectedGraph<BioVertex, BioEdge> inputDirectedGraphSif(String inputFile) {
		File myFile; BufferedReader myReader;
		DirectedGraph<BioVertex, BioEdge> myGraph = new DirectedSparseGraph<BioVertex, BioEdge>();

		try {
			// create file reader
			myFile = new File(inputFile);
			myReader = new BufferedReader(new FileReader(myFile));

    		// populate the list with nodes/edges/interactions
    		String line = myReader.readLine(); // read first line
    		String[] str = line.split("\\t"); // get nodes and interaction
    		BioVertex vS, vT; // source and target vertices
    		int id = 1; // assign an id to each vertex

    		// create the first connection, ensure graph contains 2 connected vertices
    		vS = new BioVertex(str[0], id++); vT = new BioVertex(str[2], id++);
    		myGraph.addVertex(vS); myGraph.addVertex(vT); myGraph.addEdge(new BioEdge(str[1]), vS, vT);

			while ((line = myReader.readLine()) != null) {
				str = line.split("\\t"); // get nodes and interaction

				// check if graph already contains nodes and make the connection
				vS = NetUtil.findBioVertex(myGraph, str[0]); vT = NetUtil.findBioVertex(myGraph, str[2]);
				if(vS == null) vS = new BioVertex(str[0], id++); if(vT == null) vT = new BioVertex(str[2], id++);
				myGraph.addVertex(vS); myGraph.addVertex(vT); myGraph.addEdge(new BioEdge(str[1]), vS, vT);
			}

			myReader.close();
		} catch (FileNotFoundException e) {
			// Auto-generated catch block
			e.printStackTrace();
			System.out.println("error - Input network file not found; Program will now halt");
			System.exit(-2);
		} catch (IOException e) {
			// Auto-generated catch block
			e.printStackTrace();
		}

		return myGraph;
	}

	// sets vertices based on input file
	public static <E> void inputSpecifiedVertices(Graph<BioVertex, E> g, String vFile, BioVertexStatus vStatus) {
		try {
			BufferedReader myReader = new BufferedReader(new FileReader(vFile));
			BufferedWriter myWriter = new BufferedWriter(new FileWriter("warnings.txt"));
			String line;

			while ((line = myReader.readLine()) != null) {

				String vName = null;
				String[] prot = null;
				double vExp = 0.0;
				BioVertex v = null;

				if (line.contains("=")) { // split with " = "
					prot = line.split("\\s=\\s");
				}
				else if (line.contains("\t")) { // split with tab
					prot = line.split("\\t");
				}
				else { // get only name, assume line contains only specified vertex name
					vName = line;
				}

				if (prot != null) {
					if (prot.length == 1) {
						vName = prot[0].trim();
					}
					else if (prot.length == 2) {
						vName = prot[0].trim();
						vExp = Double.parseDouble(prot[1].replace(',', '.').trim());
					}
				}
				v = NetUtil.findBioVertex(g, vName); // find BioVertex with same name

				if (v != null) {
					v.getStatus().add(vStatus);
					if (v.getStatus().contains(BioVertexStatus.SOURCE) || v.getStatus().contains(BioVertexStatus.TARGET)) { // source and/or target nodes can't be NORMAL
						v.getStatus().remove(BioVertexStatus.NORMAL);
						v.setExpLevel(vExp);
					}
				}
				else {
					System.out.println("warning - No seed node named: " + vName);
					myWriter.write("warning - No seed node named:\t" + vName); myWriter.newLine();
				}
			}

			myReader.close();
			myWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("error - Input network file not found; Program will now halt");
			System.exit(-1);
		} catch (StringIndexOutOfBoundsException e) {
			e.printStackTrace();
			System.out.println("error - Invalid specified nodes file format; Program will now halt");
			System.exit(-1);
		}
	}

	// output node legend of supplied directed graph
	public static void outputNodeLegend(Graph<BioVertex,BioEdge> g, String nodeLegendFile) {
		// sort by node ID to output to node legend
		BioVertex[] v = g.getVertices().toArray(new BioVertex[0]); // get array of all vertices in the graph
		Arrays.sort(v, new NetUtil.vertexIDComparator()); // sort array according to vertex ID

		// create legend
		try {
			BufferedWriter nWriter = new BufferedWriter(new FileWriter(nodeLegendFile));

			// write node legend
			for (int i = 0; i < v.length; i++) {
				nWriter.write(String.valueOf(v[i].getID()) + "\t" + v[i].getName());
				nWriter.newLine();
			}

			nWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void outputDirectedGraphSif(DirectedGraph<BioVertex, BioEdge> g, String outputFile) {
		try {
			BufferedWriter sifWriter = new BufferedWriter(new FileWriter(outputFile));

			// output to sif file
			for (BioEdge edj : g.getEdges()) {
				sifWriter.write(g.getSource(edj).getName() + "\t" + edj.getName() + "\t" + g.getDest(edj).getName());
				sifWriter.newLine();
			}

			sifWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void outputEdgeAttributeWeights(DirectedGraph<BioVertex, BioEdge> g, String outputFile) {
		try {
			BufferedWriter wWriter = new BufferedWriter(new FileWriter(outputFile));
			wWriter.write("EdgeWeight"); wWriter.newLine();
			NumberFormat fmt6d = new DecimalFormat("0.######", new DecimalFormatSymbols(new Locale("enUS")));

			// output to sif file
			for (BioEdge edj : g.getEdges()) {
				wWriter.write(g.getSource(edj).getName() + " (" + edj.getName() + ") " + g.getDest(edj).getName() + " = " + fmt6d.format(edj.getWeight()));
				wWriter.newLine();
			}

			wWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	// outputs a supplied directed graph to the k-walks format
	public static void outputDirecteGraphKwalks(DirectedGraph<BioVertex, BioEdge> g, String outputFile) {
		BioVertex[] v = g.getVertices().toArray(new BioVertex[0]); // get array of all vertices in the graph
		Arrays.sort(v, new NetUtil.vertexIDComparator()); // sort array according to vertex ID

		File myFile; BufferedWriter myWriter;

		try {
			// create file writer
			myFile = new File(outputFile);
			myWriter = new BufferedWriter(new FileWriter(myFile));

			// output to k-walks format
			myWriter.write(String.valueOf(v.length)); // write number of nodes
			myWriter.newLine();
			DecimalFormat df = new DecimalFormat("0.0", new DecimalFormatSymbols(new Locale("en_US"))); // to format edge weight

			for (int i = 0; i < v.length; i++) {
				myWriter.write(String.valueOf(v[i].getID())); // write current vertex ID
				BioVertex[] succesorNodes = g.getSuccessors(v[i]).toArray(new BioVertex[0]); // get array of all successor nodes
				if (succesorNodes.length > 0) {
					Arrays.sort(succesorNodes, new NetUtil.vertexIDComparator()); // sort array according to vertex ID
					for (int j = 0; j < succesorNodes.length; j++) { // write successor nodes to file
						myWriter.write(" " + String.valueOf(succesorNodes[j].getID()) + ":" + df.format(g.findEdge(v[i], succesorNodes[j]).getWeight()));
					}
				}

				myWriter.newLine();
			}
			myWriter.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// outputs a supplied directed graph to the k-walks format
	public static void outputDirecteGraphKwalks(DirectedGraph<BioVertex, BioEdge> g, String outputFile, String nodeLegendFile) {
		outputDirecteGraphKwalks(g, outputFile); // output to k-walks format file
		outputNodeLegend(g, nodeLegendFile);
	}

	// outputs the degree distribution of the supplied graph
	public static <V,E> void outputDegreeDistribution(Graph<V,E> g, NetUtil.DegreeStatus dstatus, String file) {
		double[] pk = NetUtil.calculateDegreeDistribution(g, dstatus);
		NumberFormat fmt6d = new DecimalFormat("0.######");

		try {
			BufferedWriter w = new BufferedWriter(new FileWriter(file.substring(0, file.lastIndexOf(".")) + "_" + dstatus + "degreeDist.txt"));
			w.write(dstatus + "_k\tP(k)"); w.newLine();
			for (int i = 0; i < pk.length; i++) {
				w.write(i + "\t" + fmt6d.format(pk[i])); w.newLine();
			}

			w.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// !!! WARNING - needs testing !!!, accepts as input a k-walks formatted graph
	public static DirectedGraph<BioVertex, BioEdge> inputKwalksSif(String inputFile, String nodeLegendFile, String sifFile) {
		DirectedGraph<BioVertex, BioEdge> myGraph = new DirectedSparseGraph<BioVertex, BioEdge>(); // to store input from kwalks
		DirectedGraph<BioVertex, BioEdge> sifGraph = inputDirectedGraphSif(sifFile); // Graph I get from sif

		try {
			String line;
			File nlFile = new File(nodeLegendFile); File kwFile = new File(inputFile);
			LineNumberReader legendReader = new LineNumberReader(new FileReader(nlFile)); // read legend
			LineNumberReader kwReader = new LineNumberReader(new FileReader(kwFile)); // read k-walks format

			legendReader.mark((int)(nlFile.length() + 1)); // mark beginning of files
			legendReader.skip(Long.MAX_VALUE); // skip to the end to get the number of lines
			int legendLines = legendReader.getLineNumber(); // get the line number

			String[][] lgn = new String[legendLines][2]; // create array to store legend
			legendReader.reset(); // go to the beginning of the legend file

			// read the legend file
			while ((line = legendReader.readLine()) != null) {
				lgn[legendReader.getLineNumber()] = line.split("\\t");
			}
			legendReader.close();

			line = kwReader.readLine(); kwReader.mark((int)(kwFile.length() + 1)); // first line is number of nodes, mark reading from first node
			kwReader.skip(Long.MAX_VALUE); // skip to the end to get the number of lines
			int kwLines = kwReader.getLineNumber(); kwReader.reset(); // get the total lines and reset the kwReader
			System.out.println("Node agreement in k-walks file is: " + String.valueOf(kwLines == Integer.parseInt(line) && kwLines == legendLines)); // verify that number of nodes and lines, in k-walks and legend file, are all in agreement

			if (kwLines == Integer.parseInt(line) && kwLines == legendLines) { // all lines numbers are in agreement
				for (int i = 0; i < legendLines; i++) { // add that many nodes to the graph, can use also totalNodes
					myGraph.addVertex(new BioVertex(lgn[i][1], Integer.parseInt(lgn[i][0]), BioVertex.BioVertexType.UNSPECIFIED, EnumSet.of(BioVertex.BioVertexStatus.NORMAL), myGraph)); // add vertices with name and ID
				}
			}

			// read the k-walks file
			while ((line = kwReader.readLine()) != null) {
				String[] str = line.split("\\t"); // get nodes and interaction
				BioVertex vS = NetUtil.findBioVertex(myGraph, Integer.parseInt(str[0])); // the source vertex

				for (int i = 1; i < str.length; i++) {
					String[] ni = str[i].split(":"); // name and weight
					BioVertex vT = NetUtil.findBioVertex(myGraph, Integer.parseInt(ni[0])); // the target vertex
					BioVertex sS = NetUtil.findBioVertex(sifGraph, vS); BioVertex sT = NetUtil.findBioVertex(sifGraph, vT); // corresponding vertices with same NAME in sif file
					BioEdge sEdj = sifGraph.findEdge(sS, sT); // get the edge that connects them

					myGraph.addEdge(new BioEdge(sEdj.getName(), Double.parseDouble(ni[1]), BioEdge.BioEdgeType.UNSPECIFIED), vS, vT); // add with interaction name got from sif file
				}
			}
			kwReader.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) { // Are vertices included in the network
			e.printStackTrace();
		}

		return myGraph;
	}
}
