package org.networks.subnet.bio;

import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;

import edu.uci.ics.jung.graph.Graph;

public class BioVertex {
	public boolean isFromSourceReachable;
	public boolean isTargetReaching;
	private String name; // vertex name, must be UNIQUE
	private int id; // vertex ID
	private double score; // the score of the node default: 0.0
	private double expLevel; // the expression level of the current gene
	private Collection<Double> avPathScores; //  a collection which includes the average score of all paths this vertex belongs to
	private BioVertexType type; // the type of the node
	private EnumSet<BioVertexStatus> status; // the status of the node
	private Graph<BioVertex, BioEdge> myGraph; // the main graph that this edge belongs to (not subgraph etc)
	private static boolean avPathsCalculated; // has the pathscore list been properly populated?

	public BioVertex() {
		this("unspecified", 0, BioVertexType.UNSPECIFIED, EnumSet.of(BioVertexStatus.NORMAL), null);
	}

	public BioVertex(String nameValue){
		this(nameValue, 0, BioVertexType.UNSPECIFIED, EnumSet.of(BioVertexStatus.NORMAL), null); // default ID is 0
	}

	public BioVertex(String nameValue, int idValue){
		this(nameValue, idValue, BioVertexType.UNSPECIFIED, EnumSet.of(BioVertexStatus.NORMAL), null); // default ID is 0
	}

	public BioVertex(String nameValue, int idValue, BioVertexType typeValue, EnumSet<BioVertexStatus> statusValue){
		this(nameValue, idValue, typeValue, statusValue, null);
	}

	public BioVertex(String nameValue, int idValue, BioVertexType typeValue, EnumSet<BioVertexStatus> statusValue, Graph<BioVertex, BioEdge> parentGraph){
		name = nameValue;
		id = idValue;
		status = statusValue;
		type = typeValue;
		myGraph = parentGraph;
		score = 0.0;
		avPathScores = new ArrayList<Double>();
		avPathsCalculated = false;
		isFromSourceReachable = false;
		isTargetReaching = false;
		expLevel = 0.0;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getID() {
		return id;
	}

	public void setID(int idValue) {
		this.id = idValue;
	}

	public double getScore() {
		return score;
	}

	public void setScore(double scoreValue) {
		this.score = scoreValue;
	}

	public BioVertexType getType() {
		return type;
	}

	public Collection<Double> getAvPathScores() {
		return avPathScores;
	}

	public Graph<BioVertex, BioEdge> getMyGraph() {
		return myGraph;
	}

	public static boolean getAvPathsCalculated() {
		return avPathsCalculated;
	}

	public static void setAvPathsCalculated(boolean avPathsCalculated) {
		BioVertex.avPathsCalculated = avPathsCalculated;
	}

	public EnumSet<BioVertexStatus> getStatus() {
		return status;
	}

	public void setStatus(EnumSet<BioVertexStatus> status) {
		this.status = status;
	}

	public double getExpLevel() {
		return expLevel;
	}

	public void setExpLevel(double expLevel) {
		this.expLevel = expLevel;
	}

	public String toString() {
		return "Vertex Name: " + name + ", Vertex Type: " + type + ", Vertex Status: " + getStatus();
	}

	public boolean isTraversible() { // is the node reachable at least from one source and can reach at least one target
		return isFromSourceReachable && isTargetReaching;
	}

	public double calcMaxPathScore() {
		double max = 0.0;
		if (!getAvPathScores().isEmpty()) {
			for (double sc : getAvPathScores()) {
				if (sc > max) max = sc;
			}
			return max;
		}
		else {
			return -1.0; // list was empty
		}
	}

	public double calcAvPathScore() {
		double av = 0.0;
		if (!getAvPathScores().isEmpty()) {
			for (double sc : getAvPathScores()) {
				av += sc;
			}
			return av/getAvPathScores().size();
		}
		else {
			return -1.0; // list was empty
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
	    if (getClass() == obj.getClass()){
	    	BioVertex thatBioVertex = (BioVertex)obj;

	    	 // vertices are true if they have BOTH the same name (case INsensitive)
	    	if (this.getName().equalsIgnoreCase(thatBioVertex.getName())) return true;
	    	else return false;
	    } else return false;
	}

	public enum BioVertexType {
		UNSPECIFIED, AGENT, GROWTH_FACTOR, GROWTH_FACTOR_RECEPTOR
	}

	public enum BioVertexStatus {
		NORMAL, SOURCE, TARGET
	}
}
