package org.networks.subnet.bio;

import java.util.ArrayList;
import java.util.List;

import org.networks.subnet.math.MathU;

import edu.uci.ics.jung.graph.DirectedGraph;
import edu.uci.ics.jung.graph.DirectedSparseGraph;

public class NetworkGenerator {
	// Non instantiable class, contains functions for network generation
	private NetworkGenerator() {
		throw new AssertionError();
	}

	// generator function for Erdos-Renyi networks
	public static DirectedGraph<BioVertex, BioEdge> generateDirectedER(int N, double kav) {
		double p = kav / (N - 1);

		DirectedGraph<BioVertex, BioEdge> g = new DirectedSparseGraph<BioVertex, BioEdge>();

		for (int i = 1; i <= N; i++) { // add vertices
			g.addVertex(new BioVertex("Vertex_" + i, i));
		}

		// connect vertices
		List<BioVertex> lv = new ArrayList<BioVertex>(g.getVertices());
		for (int i = 0; i < lv.size() - 1; i++) {
			for (int j = i + 1; j < lv.size(); j++) {
				if (NetUtil.rand.nextDouble() < p) {
					switch (NetUtil.rand.nextIntFromTo(0, 1)) {
					case 0:
						g.addEdge(new BioEdge(), lv.get(i), lv.get(j));
						break;
					case 1:
						g.addEdge(new BioEdge(), lv.get(j), lv.get(i));
						break;
					}
				}
			}
		}
		return g;
	}

	// generator function for Scale-Free networks
	public static DirectedGraph<BioVertex, BioEdge> generateDirectedSF(int N, int kmin, double a) {
		DirectedGraph<BioVertex, BioEdge> g = new DirectedSparseGraph<BioVertex, BioEdge>();

		for (int i = 1; i <= N; i++) { // add vertices
			g.addVertex(new BioVertex("Vertex_" + i, i));
		}

		List<BioVertex> lv = new ArrayList<BioVertex>(g.getVertices());
		List<BioVertex> li = new ArrayList<BioVertex>(); // in links
		List<BioVertex> lo = new ArrayList<BioVertex>(); // out links

		// add each vertex to the input/output lists as many times as its degree (drawn from power law distribution)
		for(BioVertex v : lv) {
			int links = (int)MathU.randomPow(kmin, N, a, NetUtil.rand);
			for (int i = 1; i <= links; i++) {
				switch (NetUtil.rand.nextIntFromTo(0, 1)) {
				case 0:
					li.add(v);
					break;
				case 1:
					lo.add(v);
					break;
				}
			}
		}

		int fails = 0;
		int allowedFails = (li.size() + lo.size()) * 5;

		// connect vertices
		while (fails < allowedFails && !lo.isEmpty() && !li.isEmpty()) {
			BioVertex vo = lo.get(NetUtil.rand.nextIntFromTo(0, lo.size() - 1));
			BioVertex vi = li.get(NetUtil.rand.nextIntFromTo(0, li.size() - 1));

			BioEdge e = new BioEdge();

			if (g.addEdge(e, vo, vi)) { // vertices are connected successfully
				lo.remove(vo);
				li.remove(vi);
			}
			else {
				fails++; // number of failures increases
			}
		}

		// remove vertices with degree 0
		for(BioVertex v : lv) {
			if (g.degree(v) == 0)  g.removeVertex(v);
		}

		return g;
	}
}
