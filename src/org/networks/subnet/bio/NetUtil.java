package org.networks.subnet.bio;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.networks.subnet.bio.BioEdge.BioEdgeType;
import org.networks.subnet.bio.BioVertex.BioVertexStatus;
import org.networks.subnet.math.DependencyMatrixEntity;

import cern.jet.random.Uniform;
import cern.jet.random.engine.MersenneTwister;
import edu.uci.ics.jung.algorithms.cluster.WeakComponentClusterer;
import edu.uci.ics.jung.algorithms.shortestpath.DijkstraDistance;
import edu.uci.ics.jung.graph.DirectedGraph;
import edu.uci.ics.jung.graph.DirectedSparseGraph;
import edu.uci.ics.jung.graph.Graph;

public class NetUtil {
	public static Uniform rand = new Uniform(new MersenneTwister()); // uniform random number generator, based on Mersenne Twister

	// Non instantiable utility class, utility function for graphs
	private NetUtil() {
		throw new AssertionError();
	}

	// returns the vertex in with same name, else returns null, Graph must contain at least one Vertex
	public static <E> BioVertex findBioVertex(Graph<BioVertex, E> g, String vertexName){
		for (BioVertex v : g.getVertices()) {
			if(v.getName().equals(vertexName)) { // if v name is already in the network
				return v;
			}
		}
		return null; // return null, v is not present in the network
	}

	// returns the vertex in the graph with the same ID, else returns null, Graph must contain at least one Vertex
	public static <E> BioVertex findBioVertex(Graph<BioVertex, E> g, int vertexID){
		for (BioVertex v : g.getVertices()) {
			if(v.getID() == vertexID) { // if v name is already in the network
				return v;
			}
		}
		return null; // return null, v is not present in the network
	}

	// returns the vertex in the graph with the same name, else returns null, Graph must contain at least one Vertex
	public static <E> BioVertex findBioVertex(Graph<BioVertex, E> g, BioVertex thisVertex){
		for (BioVertex v : g.getVertices()) {
			if(v.equals(thisVertex)) { // if v name is already in the network
				return v;
			}
		}
		return null; // return null, v is not present in the network
	}

	// returns the subset of source vertices that reach at least one target, assumes vertex reachability has been discovered
	public static List<BioVertex> findReachingVertices(DirectedGraph<BioVertex, BioEdge> g) {
		List<BioVertex> srcReachingVertices = new ArrayList<BioVertex>();

		for (BioVertex v : g.getVertices()) {
			if (v.getStatus().contains(BioVertexStatus.SOURCE) && v.isTargetReaching) {
				srcReachingVertices.add(v);
			}
		}

		return srcReachingVertices;
	}

	// returns the maximum degree of the provided graph
	public static <V, E> int findMaxDegree(Graph<V,E> g, DegreeStatus dstatus) {
		int m = 0;
		for(V v : g.getVertices()) {
			switch (dstatus) {
			case IN:
				if(g.inDegree(v) > m) m = g.inDegree(v);
				break;
			case OUT:
				if(g.outDegree(v) > m) m = g.outDegree(v);
				break;
			case INOUT:
				if(g.degree(v) > m) m = g.degree(v);
				break;
			}
		}
		return m;
	}

	// returns the minimum degree of the provided graph
	public static <V, E> int findMinDegree(Graph<V,E> g, DegreeStatus dstatus) {
		int m = g.getVertices().size() - 1; // kmax = N-1
		for(V v : g.getVertices()) {
			switch (dstatus) {
			case IN:
				if(g.inDegree(v) < m) m = g.inDegree(v);
				break;
			case OUT:
				if(g.outDegree(v) < m) m = g.outDegree(v);
				break;
			case INOUT:
				if(g.degree(v) < m) m = g.degree(v);
				break;
			}
		}
		return m;
	}

	// returns the normalized degree distribution of the provided graph
	public static <V, E> double[] calculateDegreeDistribution(Graph<V,E> g, DegreeStatus dstatus) {
		double[] pk = new double[findMaxDegree(g, dstatus) + 1]; // degree distribution

		for(V v : g.getVertices()) { // set degree distribution
			switch (dstatus) {
			case IN:
				pk[g.inDegree(v)]++;
				break;
			case OUT:
				pk[g.outDegree(v)]++;
				break;
			case INOUT:
				pk[g.degree(v)]++;
				break;
			}
		}

		int N = g.getVertices().size();
		for (int i = 0; i < pk.length; i++) { // normalize
			pk[i] /= N;
		}
		return pk;
	}

	// marks all vertices as reachable from source and reaching at least one target
	public static void findVertexReachability(DirectedGraph<BioVertex, BioEdge> g, BioVertexStatus srcStatus, BioVertexStatus tgtStatus) {
		List<BioVertex> srcVertices = NetUtil.getSpecifiedVertices(g, srcStatus);
		List<BioVertex> tgtVertices = NetUtil.getSpecifiedVertices(g, tgtStatus);

		DijkstraDistance<BioVertex, BioEdge> dist = new DijkstraDistance<BioVertex, BioEdge>(g, false); // object to calculate distances

		for (BioVertex v : g.getVertices()) { // set all vertices as non-reachable by default, will discover reachability
			v.isFromSourceReachable = false;
			v.isTargetReaching = false;
		}

		for (BioVertex s : srcVertices) { // calculate reachability from source
			Map<BioVertex, Number> dm = dist.getDistanceMap(s);

			for (BioVertex t : dm.keySet()) {
				if (s != t) t.isFromSourceReachable = true;
			}
		}

		for (BioVertex s : g.getVertices()) { // calculate reachability to target
			Map<BioVertex, Number> dm = dist.getDistanceMap(s);

			for (BioVertex t : tgtVertices) {
				if (dm.keySet().contains(t) && s != t) {
					s.isTargetReaching = true;
				}
			}
		}
	}

	// resets the vertex ID's to ensure they are in incremental order
	public static void resetVertexIDs(Graph<BioVertex, BioEdge> g) {
		BioVertex[] v = g.getVertices().toArray(new BioVertex[0]); // get array of all vertices in the graph
		Arrays.sort(v, new NetUtil.vertexIDComparator()); // sort array according to vertex ID

		for (int i = 0; i < v.length; i++) {
			v[i].setID(i + 1);
		}
	}

	// select a random node from a list of provided vertices
	public static <V> V selectRandomVertex(List<V> l) {
		int r = 0;
		try {
			r = rand.nextIntFromTo(0, l.size() - 1);
		} catch (IndexOutOfBoundsException e) {
			System.out.println("error - Attempted to select a random node from empty list; Program will now halt");
			System.exit(-1);
		}

		return l.get(r);
	}

	// select a random seed node
	public static <E> BioVertex selectRandomSpecifiedVertex(Graph<BioVertex, E> g, BioVertexStatus vertexStatus) {
		return selectRandomVertex(getSpecifiedVertices(g, vertexStatus));
	}

	// sets a random number of nodes as both source and target
	public static <E> List<BioVertex> setRandomSpecifiedVertices(Graph<BioVertex, E> g, int vNumber) {
		List<BioVertex> l = new ArrayList<BioVertex>(g.getVertices());
		List<BioVertex> l2 = new ArrayList<BioVertex>();

		for (int i = 0; i < vNumber; i++) {
			BioVertex v = selectRandomVertex(l);
			v.getStatus().remove(BioVertexStatus.NORMAL);
			v.getStatus().add(BioVertexStatus.SOURCE);
			v.getStatus().add(BioVertexStatus.TARGET);
			l.remove(v);
			l2.add(v);
		}

		return l2;
	}

	// return all the nodes of the provided vertex status
	public static <E> List<BioVertex> getSpecifiedVertices(Graph<BioVertex, E> g, BioVertexStatus vertexStatus) {
		List<BioVertex> l = new ArrayList<BioVertex>();

		for (BioVertex v : g.getVertices()) {
			if (v.getStatus().contains(vertexStatus)) l.add(v);
		}

		return l;
	}

	// return all the seed nodes in a JGraphT graph object
	public static <E> List<BioVertex> getSpecifiedVertices(org.jgrapht.Graph<BioVertex, E> jg, BioVertexStatus vertexStatus) {
		List<BioVertex> l = new ArrayList<BioVertex>();

		for (BioVertex v : jg.vertexSet()) {
			if (v.getStatus().contains(vertexStatus)) l.add(v);
		}

		return l;
	}

	// check if all seed nodes belong to a weakly connected component
	public static boolean checkSpecifiedNodesWeaklyConnected(DirectedGraph<BioVertex, BioEdge> g, BioVertexStatus srcStatus, BioVertexStatus tgtStatus) {
		Set<BioVertex> sv = new HashSet<BioVertex>();
		sv.addAll(NetUtil.getSpecifiedVertices(g, srcStatus));
		sv.addAll(NetUtil.getSpecifiedVertices(g, tgtStatus));

		WeakComponentClusterer<BioVertex, BioEdge> wClusterer = new WeakComponentClusterer<BioVertex, BioEdge>();
		Set<Set<BioVertex>> wClust = wClusterer.transform(g);

		for (Set<BioVertex> w : wClust) {
			if (w.containsAll(sv)) return true; // there is one weakly connected component containing all seed vertices
		}
		return false;
	}

	// get JGraphT graph from JUNG Graph
	public static org.jgrapht.DirectedGraph<BioVertex, BioEdge> convertDirectedGraph(DirectedGraph<BioVertex, BioEdge> g) {
		org.jgrapht.DirectedGraph<BioVertex, BioEdge> jg = new org.jgrapht.graph.DefaultDirectedGraph<BioVertex, BioEdge>(BioEdge.class);
		for (BioVertex v : g.getVertices()) {
			jg.addVertex(v);
		}

		for (BioEdge e : g.getEdges()) {
			jg.addEdge(g.getSource(e), g.getDest(e), e);
		}

		return jg;
	}

	// get JUNG graph from JGrapht Graph
	public static DirectedGraph<BioVertex, BioEdge> convertDirectedGraph(org.jgrapht.DirectedGraph<BioVertex, BioEdge> g) {
		DirectedGraph<BioVertex, BioEdge> jg = new DirectedSparseGraph<BioVertex, BioEdge>();
		for (BioVertex v : g.vertexSet()) {
			jg.addVertex(v);
		}

		for (BioEdge e : g.edgeSet()) {
			jg.addEdge(e, g.getEdgeSource(e), g.getEdgeTarget(e));
		}

		return jg;
	}


	// copy network (deep copy)
	public static DirectedGraph<BioVertex, BioEdge> copyDirectedGraph(DirectedGraph<BioVertex, BioEdge> g, boolean deep) {
		DirectedGraph<BioVertex, BioEdge> jg = new DirectedSparseGraph<BioVertex, BioEdge>();

		for (BioVertex v : g.getVertices()) {
			if (deep) {
				BioVertex nv = new BioVertex(v.getName(), v.getID(), v.getType(), v.getStatus());
				nv.setScore(v.getScore());
				jg.addVertex(nv);
			}
			else {
				jg.addVertex(v);
			}
		}

		for (BioEdge e : g.getEdges()) {
			if (deep) {
				BioEdge ne = new BioEdge(e.getName(), e.getWeight(), e.getType());
				ne.setScore(e.getScore());
				jg.addEdge(ne, NetUtil.findBioVertex(jg, g.getSource(e)), NetUtil.findBioVertex(jg, g.getDest(e)));
			}
			else {
				jg.addEdge(e, g.getSource(e), g.getDest(e));
			}
		}

		return jg;
	}

	// copy network (default=shallow copy)
	public static DirectedGraph<BioVertex, BioEdge> copyDirectedGraph(DirectedGraph<BioVertex, BioEdge> g) {
		return copyDirectedGraph(g, false);
	}

	// converts a depedency matrix to a directed weighted graph, with edgeWeight as distance or probability
	public static DirectedGraph<BioVertex, BioEdge> convertDependencyMatrix(DependencyMatrixEntity[][] dm, String edgeWeight) {
		DirectedGraph<BioVertex, BioEdge> g = new DirectedSparseGraph<BioVertex, BioEdge>();

        for (int i = 0; i < dm.length; i++) {
            for (int j = 0; j < dm[i].length; j++) {
            	BioVertex s = dm[i][j].sourceVertex;
            	BioVertex t = dm[i][j].targetVertex;

            	if (!g.containsVertex(s)) g.addVertex(s);
            	if (!g.containsVertex(t)) g.addVertex(t);
            	if (!g.isNeighbor(s, t)) { // is not already connected
            		if (dm[i][j].probability > 0.0 && edgeWeight.equals("probability")) {
                		g.addEdge(new BioEdge("unknown", dm[i][j].probability, BioEdgeType.UNSPECIFIED), s, t); // add edge with probability as weight
            		}
            		if (dm[i][j].distance > 0 && edgeWeight.equals("distance")) {
                		g.addEdge(new BioEdge("unknown", dm[i][j].distance, BioEdgeType.UNSPECIFIED), s, t); // add edge with distance as weight
            		}
            	}
            }
        }

		return g;
	}

	// nested comparator classes,
	public static class vertexInDegreeComparator implements Comparator<BioVertex> { // comparator according to in Degree
		@Override
		public int compare(BioVertex o1, BioVertex o2) {
			if (((DirectedGraph<BioVertex,BioEdge>)o1.getMyGraph()).inDegree(o1) < ((DirectedGraph<BioVertex,BioEdge>)o2.getMyGraph()).inDegree(o2)) {
				return -1;
			} else if (((DirectedGraph<BioVertex,BioEdge>)o1.getMyGraph()).inDegree(o1) > ((DirectedGraph<BioVertex,BioEdge>)o2.getMyGraph()).inDegree(o2)) {
				return 1;
			} else {
				return 0;
			}
		}
	}

	public static class vertexOutDegreeComparator implements Comparator<BioVertex> { // comparator according to out Degree
		@Override
		public int compare(BioVertex o1, BioVertex o2) {
			if (((DirectedGraph<BioVertex,BioEdge>)o1.getMyGraph()).outDegree(o1) < ((DirectedGraph<BioVertex,BioEdge>)o2.getMyGraph()).outDegree(o2)) {
				return -1;
			} else if (((DirectedGraph<BioVertex,BioEdge>)o1.getMyGraph()).outDegree(o1) > ((DirectedGraph<BioVertex,BioEdge>)o2.getMyGraph()).outDegree(o2)) {
				return 1;
			} else {
				return 0;
			}
		}
	}

	public static class vertexDegreeComparator implements Comparator<BioVertex> { // comparator according to Degree (for directed and undirected graphs)
		@Override
		public int compare(BioVertex o1, BioVertex o2) {
			if (((Graph<BioVertex,BioEdge>)o1.getMyGraph()).degree(o1) < ((Graph<BioVertex,BioEdge>)o2.getMyGraph()).degree(o2)) {
				return -1;
			} else if (((Graph<BioVertex,BioEdge>)o1.getMyGraph()).degree(o1) > ((Graph<BioVertex,BioEdge>)o2.getMyGraph()).degree(o2)) {
				return 1;
			} else {
				return 0;
			}
		}
	}

	public static class vertexIDComparator implements Comparator<BioVertex> { // comparator according to vertex ID
		@Override
		public int compare(BioVertex o1, BioVertex o2) {
			return Integer.compare(o1.getID(), o2.getID());
		}
	}

	public static class vertexNameComparator implements Comparator<BioVertex> { // comparator according to vertex name
		@Override
		public int compare(BioVertex o1, BioVertex o2) {
			return o1.getName().compareTo(o2.getName());
		}
	}

	public static class vertexExpressionComparator implements Comparator<BioVertex> { // comparator according to ABSOLUTE value of gene expression
		@Override
		public int compare(BioVertex o1, BioVertex o2) {
			return Double.compare(Math.abs(o1.getExpLevel()), Math.abs(o2.getExpLevel()));
		}

	}

	public static class matrixTargetExpressionComparator implements Comparator<DependencyMatrixEntity> { // comparator according to ABSOLUTE value of gene expression
		@Override
		public int compare(DependencyMatrixEntity o1, DependencyMatrixEntity o2) {
			return Double.compare(Math.abs(o1.targetVertex.getExpLevel()), Math.abs(o2.targetVertex.getExpLevel()));
		}
	}

	public static class matrixSourceExpressionComparator implements Comparator<DependencyMatrixEntity> { // comparator according to ABSOLUTE value of gene expression
		@Override
		public int compare(DependencyMatrixEntity o1, DependencyMatrixEntity o2) {
			return Double.compare(Math.abs(o1.sourceVertex.getExpLevel()), Math.abs(o2.sourceVertex.getExpLevel()));
		}
	}

	public static class targetScoreComparator implements Comparator<DependencyMatrixEntity> {
		@Override
		public int compare(DependencyMatrixEntity o1, DependencyMatrixEntity o2) {
			return Double.compare(o1.targetScore, o2.targetScore);
		}
	}

	public static class sourceScoreComparator implements Comparator<DependencyMatrixEntity[]> {
		@Override
		public int compare(DependencyMatrixEntity[] o1, DependencyMatrixEntity[] o2) {
			return Double.compare(o1[0].sourceScore, o2[0].sourceScore);
			}
	}

	public enum DegreeStatus {
		IN, OUT, INOUT
	}
}