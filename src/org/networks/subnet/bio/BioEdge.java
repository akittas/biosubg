package org.networks.subnet.bio;

public class BioEdge {
	private String name; // edge name, default: unspecified
	private double weight; // edge weight, default: 1.0;
	private double score; // the score of the link default: 0.0
	private BioEdgeType type; // the type of the edge

	public BioEdge() {
		this("unspecified", 1.0, BioEdgeType.UNSPECIFIED);
	}

	public BioEdge(String interactionName){
		this(interactionName, 1.0, BioEdgeType.UNSPECIFIED);
	}

	public BioEdge(String interactionName, double weightValue, BioEdgeType typeValue){
		name = interactionName;
		weight = weightValue;
		type = typeValue;
		score = 0.0;
	}

	public void increaseScore(double sc) {
		score += sc;
	}

	public void decreaseScore(double sc) {
		score -= sc;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public double getScore() {
		return score;
	}

	public void setScore(double scoreValue) {
		this.score = scoreValue;
	}

	public BioEdgeType getType() {
		return type;
	}

	public String toString() {
		return "Edge Name: " + name + ", Edge Type: " + type + "Edge Weight: " + weight;
	}

	public enum BioEdgeType {
		UNSPECIFIED, TRANSITION, ACTIVATION, INHIBITION
	}
}
