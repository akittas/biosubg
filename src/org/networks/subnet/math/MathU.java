package org.networks.subnet.math;

import cern.jet.random.Uniform;

public class MathU {

	// Non instantiable utility class, math utility functions
	private MathU() {
		throw new AssertionError();
	}

	// returns a random number with a power law distribution P(x)=x^a in [min, max)
	public static double randomPow(double min, double max, double a, Uniform rand) {
        double ap = 1.0 + a;
        double expr = (Math.pow(max, ap) - Math.pow(min, ap)) * rand.nextDouble() + Math.pow(min, ap);
        return Math.pow(expr, 1.0 / ap) - 1E-12; // ensure it stays < max
	}
}
