package org.networks.subnet.math;

import java.io.*;
import java.util.*;

public class NetCombiTest {
	// Non-instantiable utility class
	private NetCombiTest() {
		throw new AssertionError();
	}

	public static void RandomizeInputNodes(File inputFile, File outputFile, int nodeNumber) {
		List<String> myList; // list to store the nodes
		try {
			BufferedReader inputReader = new BufferedReader(new FileReader(inputFile)); // create file reader
			myList = new ArrayList<String>();
    		String line;

			while ((line = inputReader.readLine()) != null) {
				myList.add(line);
			}
			inputReader.close();
		} catch (FileNotFoundException e) {
			// Auto-generated catch block
			e.printStackTrace();
			return;
		} catch (IOException e) {
			// Auto-generated catch block
			e.printStackTrace();
			return;
		}
	}

	public static int factorial(int n) {
	    if (n == 0) {
	        return 1;
	    } else {
	        return n * factorial(n - 1);
	    }
	}


}
