package org.networks.subnet.math;

import org.networks.subnet.bio.BioVertex;

public class DependencyMatrixEntity {
	public BioVertex sourceVertex;
	public BioVertex targetVertex;
	public double sourceScore;
	public double targetScore;
	public double probability;
	public int distance;
	public double avSourceDistance;
	public double avTargetDistance;

	public DependencyMatrixEntity() {
		sourceVertex = null;
		targetVertex = null;
		sourceScore = 0.0;
		targetScore = 0.0;
		probability = 0.0;
		distance = 0;
		avSourceDistance = 0.0;
		avTargetDistance = 0.0;
	}
}
