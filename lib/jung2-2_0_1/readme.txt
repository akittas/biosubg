JUNG 2.0 Framework contains both binary and source form of jars.

To use the binary jars from JUNG, you need to add them as Referenced Libraries in a project. Create a new project. Then, add required JUNG jars somewhere inside project directory. Refresh project tree. Right-click on each JUNG jar to add it to Build Path > Add to Build Path in Eclipse. By doing this, each will be registered under Referenced Libraries tree in the project.

Since a few JUNG jars have source code version, you can open the Build Path of the project and go to Libraries tab. Under this tab, it lists all JARs. Attach the JUNG source code jar version to equivalent JUNG binary jar version under Source attachment of that jar. For example: attach jung-samples-2.0.1-sources.jar to jung-samples-2.0.1.jar

Once done, go to jung-samples-2.0.1 under Referenced Libraries and open any class in a JUNG package. You will be able to see the source code view of the class.
